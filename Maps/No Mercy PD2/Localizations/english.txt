{
	"PD2_heist_nomercy" : "No Mercy (Payday 2 version)"
	"PD2_nomercy_briefing" : "We are hitting up the Mercy hospital in a heist for blood. The source is carrying some kind of rare virus and we need to get it out of him. Let nothing stop us as the paycheck is a hefty one. Spilling some blood for this kind of cash is not the end of the world."
	"no_mercy_intro" : "Bain: When you put your mask on you will have 7 seconds to take out the cameras. I will start counting down once you have put your mask on.",
	"no_mercy_countdown_7" : "Bain: 7",
	"no_mercy_countdown_6" : "Bain: 6",
	"no_mercy_countdown_5" : "Bain: 5",
	"no_mercy_countdown_4" : "Bain: 4",
	"no_mercy_countdown_3" : "Bain: 3",
	"no_mercy_countdown_2" : "Bain: 2",
	"no_mercy_countdown_1" : "Bain: 1",
	"no_mercy_stealth" : "Bain: Keep the civilians down and watch the phone.",
	"no_mercy_loud" : "Bain: Looks like it went loud, you need to get the saw and saw through the doors.",
	"no_mercy_phone" : "Bain: Answer the phone!",
	"no_mercy_guard" : "Bain: A security guard is coming to your location, take him out silently.",
	"debug_interact_hospital_veil_hold" : "Hold $BTN_INTERACT to Take a Blood Sample"
	"debug_interact_sample_validation" : "Press $BTN_INTERACT to Insert the Blood Sample"
	"debug_interact_hospital_veil_take" : "Press $BTN_INTERACT to Take the Blood Sample"
}